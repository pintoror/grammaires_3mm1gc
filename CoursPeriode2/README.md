# CTD de période 2 (introduction au traitement informatique des langages informatiques)

Le cours (comme d'autres cours de l'Ensimag) s'appuie sur le vocabulaire de la théorie des ensembles.
Ce [document](equipotence.pdf) en détaille les idées de base. Travailler ce document n'est pas strictement indispensable pour l'examen. Mais ce sera très utile pour avoir une meilleure compréhension (sic!) du langage de programmation Python, de l'informatique et des mathématiques. Un corrigé de ses exercices est fourni [ici](equipotence-correction.pdf).

Les cours sont organisés en "chapitres".

- Chapitre 1: introduction aux concepts de base. [diapos 1up](chap1.pdf) ou [diapos 4up](chap1-4up.pdf) 
- Chapitre 2: définition des langages algébriques (et BNF). [diapos 1up](chap2.pdf) ou [diapos 4up](chap2-4up.pdf) 
- Chapitre 3: théorie et applications des syntaxes abstraites (et des syntaxes préfixes). [diapos 1up](chap3.pdf) ou [diapos 4up](chap3-4up.pdf) 
- Chapitre 4: Sémantiques des BNF attribuées et arbres d'analyse [diapos 1up](chap4.pdf) ou [diapos 4up](chap4-4up.pdf) 
- Chapitre 5: théorie des grammaires et de l'analyse syntaxique LL(1) [diapos 1up](chap5.pdf) ou [diapos 4up](chap5-4up.pdf), avec [feuille d'exos sur l'analyse syntaxique LL(1)](tds-parsing.pdf) et sa [correction](tds-parsing-correction.pdf).
