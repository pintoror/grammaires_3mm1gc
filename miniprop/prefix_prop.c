/* Implementation of a parser in prefix notation */

#include <stdio.h>
#include "lexer.h"
#include "cmdline.h"
#include "prop.h"

static void parse_rec(Prop *p) {
        int v;
        token current = next(&v);
        switch (current) {
        case TRUE:
                *p = true;
                break;
        /* A COMPLETER: AUTRES CAS */ 
        default:
                unexpected(current, v, "in a proposition");              
        }
}

static Prop parse() {
        int v;
        token current;
        Prop p;
        parse_rec(&p);
        current = next(&v);
        if (current != END){
                unexpected(current, v, "after the proposition");                
        }
        return p;
}

int main(int argc, char *argv[]) {
        run(&parse, "prefix", argc, argv);
        return 0;
}
