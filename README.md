# Grammaires et Compilation 2022-2023 -- périodes 2 et 3 (Ensimag 1A Apprentissage) 

auteur: [Sylvain Boulmé](mailto:Sylvain.Boulme@univ-grenoble-alpes.fr)

[site gitlab](https://gricad-gitlab.univ-grenoble-alpes.fr/boulme/grammaires_3mm1gc)

## Période 2: introduction au traitement informatique des langages informatiques

- [fichiers fournis du TP](miniprop/) et [sujet](miniprop/sujet.pdf)
- [diapos des CTD et énoncé des TDs](CoursPeriode2/)

