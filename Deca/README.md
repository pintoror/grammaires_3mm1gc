# Compilation du langage Deca (sujet du [projet GL](https://chamilo.grenoble-inp.fr/courses/ENSIMAG3MM1PGL))

- [Spécification de Deca et doc du projet GL](projetGL_app.pdf). **Il est conseillé d'imprimer** cette [sélection de pages](projetGL_app_selection-nup.pdf) pour [l'examen](../Exams/juin2022.pdf).
  Pour trouver les mises à jour importantes de la doc depuis le début de projet, vous pouvez chercher le texte "`[MODIF`" dans le document (ou essayer la commande unix `diffpdf`).
- **Feuille TD 1:** [Introduction à la compilation de Deca sans objet](td_intro_decac.pdf) et son [corrigé](td_intro_decac-correction.pdf). 
- **Feuille TD 2:** [Introduction à la génération de code du Deca sans objet](td_intro_gencode.pdf) et son [corrigé](td_intro_gencode-correction.pdf). 
- **Feuille TD 3:** [Introduction à la sémantique des objets en Deca](td_intro_objects.pdf) et son [corrigé](td_intro_objects-correction.pdf). 
- **Feuille TD 4:** [Diapos sur la génération de code Deca objet](diapos-gencode-objet.pdf). 
